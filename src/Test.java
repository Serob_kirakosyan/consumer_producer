public class Test  {
    public  int count = -2;
     Integer [] array = new Integer[10];
     boolean flag = true;
    static Test test = new Test();

    public boolean add(Integer eiem) {
        int arrayLastElem = array.length - 1;
        if (flag) {
            shift();
            array[arrayLastElem] = eiem;
            count++;
        } else {
            return false;
        }
        if (count == arrayLastElem) {
            flag = false;
        }
        return true;
    }

    public Integer get(){
        if (!flag){
            Integer eiem = array[0];
            array[0] = null;
            shift();
            if (count == 0){
                flag = true;
            }
            count--;
            return eiem;
        }
        return null;
    }

    private void shift(){
        for (int i = 0;i < array.length - 1; i++){
            Integer tmp = array[i + 1];
            array[i + 1] = array[i];
            array[i] = tmp;
        }
    }


    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(new Produser());
        Thread t2 = new Thread(new Customer());
        t1.start();
        t2.start();

    }
}
